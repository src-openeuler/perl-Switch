Name:           perl-Switch
Version:        2.17
Release:        14
Summary:        A switch statement for Perl
License:        GPL-1.0-or-later or Artistic-1.0
URL:            https://metacpan.org/release/Switch
Source0:        https://cpan.metacpan.org/authors/id/C/CH/CHORNY/Switch-2.17.tar.gz
BuildRequires:  make perl-interpreter perl-generators perl(ExtUtils::MakeMaker)
BuildArch:      noarch

%description
Switch.pm provides the syntax and semantics for an explicit case mechanism for Perl.
The syntax is minimal, introducing only the keywords C<switch> and C<case> and conforming
to the general pattern of existing Perl control structures. The semantics are particularly
rich, allowing any one (or more) of nearly 30 forms of matching to be used when comparing
a switch value with its various cases.

%package        help
Summary:        Help document for perl-Switch

%description    help
Help document for perl-Switch.

%prep
%autosetup -n Switch-%{version} -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
%make_build

%install
%make_install
chmod -R u+w %{buildroot}/*

%check
make test

%files
%doc README Changes
/usr/share/perl5/vendor_perl/Switch.pm
%exclude /usr/lib64/perl5/vendor_perl/auto/Switch/.packlist
%exclude /usr/lib64/perl5/perllocal.pod

%files help
/usr/share/man/man3/Switch.3pm.gz

%changelog
* Thu Jul 14 2022 Chenyx <chenyixiong3@huawei.com> - 2.17-14
- License compliance rectification

* Mon Jan 06 2020 shijian <shijian16@huawei.com> - 2.17-13
- Package init
